package be.meulemans;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping(
        value = "/private",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public class PrivateApiController {
    private static final Logger logger = LoggerFactory.getLogger(PrivateApiController.class);

    @GetMapping("/values")
    public String[] getValues() {
        logger.debug("Values requested");
        return new String[]{"four", "five"};
    }

    @PostMapping("/values")
    public void postValues(String[] values) {
        logger.debug("Provided values: {}", Arrays.toString(values));
    }

}
